/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.darkandlightstudio;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nakou | Twitter : Nakou - Mail : "nakou01[at]gmail.com"
 */
public class DataHandler implements Runnable{

    private DataTweet data;
    
    public DataHandler(DataTweet d){
        data = d;
    }
    
    @Override
    public void run() {
        data = new DataTweet(true);
        while(true){
            if(!data.isOkay())
                System.out.print("Data unaviable, retry soon...");
            else{
                data.refreshData();
            }
            try {
                Thread.sleep(150000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DataHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
