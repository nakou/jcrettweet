/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.darkandlightstudio;

import java.util.ArrayList;

/**
 *
 * @author Nakou | Twitter : Nakou - Mail : "nakou01[at]gmail.com"
 */
public class DataTweet {
    private ArrayList<Tweet> tweetHomeDataList;
    private ArrayList<Tweet> tweetMentionDataList;
    private ArrayList<Tweet> tweetMeDataList;
    private String User;
    private String pass;
    private String apiKey;
    private int cacheSize;
    
    /**
     * TODO
     */
    public DataTweet(){
        
    }
    
    /**
     * 
     * @return true if data are aviable. False else.
     * TODO
     */
    public boolean isOkay(){
        return true;
    }
    
    /**
     * 
     * @param b Just change signature.
     * TODO
     */
    public DataTweet(boolean b){ // For Simulation
        tweetHomeDataList = new ArrayList<>();
        this.User = "@Nakou";
        callData();
    }

    /**
     * 
     */
    public void refreshData(){
        purgeData();
        callData();
    }
    
    /**
     * Call datas amount from the cachesize.
     * TODO
     */
    public void callData(){
        System.out.print("Call new data");
        tweetHomeDataList.add(new Tweet("@Feilyn","Meow meow","12/12/12 - 12h12"));
        tweetHomeDataList.add(new Tweet("@Baeshee","PUTAIN GALLANT!","13/12/12 - 13h12"));
        tweetHomeDataList.add(new Tweet("@Gallant_thief","Je vais... invisible...","13/12/12 - 14h12"));
        tweetHomeDataList.add(new Tweet("@Gallant_thief","Je vais... invisible...","13/12/12 - 14h12"));
        tweetHomeDataList.add(new Tweet("@Gallant_thief","Je vais... invisible...","13/12/12 - 14h12"));
        tweetHomeDataList.add(new Tweet("@Gallant_thief","Je vais... invisible...","13/12/12 - 14h12"));
        tweetHomeDataList.add(new Tweet("@Gallant_thief","Je vais... invisible...","13/12/12 - 14h12"));
        tweetHomeDataList.add(new Tweet("@Gallant_thief","Je vais... invisible...","13/12/12 - 14h12"));
        tweetHomeDataList.add(new Tweet("@Gallant_thief","Je vais... invisible...","13/12/12 - 14h12"));
        tweetHomeDataList.add(new Tweet("@Gallant_thief","Je vais... invisible...","13/12/12 - 14h12"));
        tweetHomeDataList.add(new Tweet("@Nakou","Ceci est un tweet d'une longueur maximum de 140 caractère. C'est court, mais pas trop, on peut en dire beaucoup, et sur le ton qu'on veux...","13/12/12 - 14h12"));
    }
    
    /**
     * Purge olds tweet from the last call to catch another list.
     * TODO
     */
    public void purgeData(){
        System.out.print("Purge all data");
        tweetHomeDataList.removeAll(tweetHomeDataList);
    }
    
    public ArrayList<Tweet> callHomeTweets(){
        return this.tweetHomeDataList;
    }

    public ArrayList<Tweet> callMentionTweets(){
        return this.tweetMentionDataList;
    }

    public ArrayList<Tweet> callMeTweets(){
        return this.tweetMeDataList;
    }    
}
