/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.darkandlightstudio;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nakou | Twitter : Nakou - Mail : "nakou01[at]gmail.com"
 */
public class MainApp implements Runnable{

    // Init Account variables
    private String userName = "Nakou";
    private String pass;
    private DataTweet data = new DataTweet(true);
    
    // Create TrayIcon
    private Toolkit toolkit = Toolkit.getDefaultToolkit();
    private PopupMenu popup = new PopupMenu();
    private Image img = toolkit.getImage("res/img/icon.gif");
    private TrayIcon trayIcon = new TrayIcon(img);
    private SystemTray tray = SystemTray.getSystemTray();
            
    // Create a pop-up menu components
    private MenuItem aboutItem = new MenuItem("About");
    private Menu dmMenu = new Menu("Direct Messages");
    private Menu mentionsItem = new Menu("Connect");
    private Menu tweetsItem = new Menu("Home");
    private Menu youItem = new Menu("Me");
    private MenuItem exitItem = new MenuItem("Exit");
    
    
    private boolean isSystraySupport(){
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return false;
        } else
            return true;
    }
    
    private void constructMenu(){
            //Add components to pop-up menu

            popup.add(userName);
            popup.add(tweetsItem);
            popup.add(mentionsItem);
            popup.add(youItem);
            popup.addSeparator();
            popup.add(dmMenu);
            popup.addSeparator();
            popup.add(aboutItem);
            popup.add(exitItem);
            trayIcon.setPopupMenu(popup);
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.out.println("TrayIcon could not be added.");
            }
    }
    
    public void feedTweets(){
        for(Tweet t : data.callHomeTweets()){
            tweetsItem.add("---------------");
            if(!(t.getContent().length() > 36))
                tweetsItem.add(t.getContent());
            else{
                tweetsItem.add(t.getContent().substring(0, 36));
                if(t.getContent().length() > 72){
                    tweetsItem.add(t.getContent().substring(36,72));
                    if(t.getContent().length() > 108){
                        tweetsItem.add(t.getContent().substring(72,108));
                        tweetsItem.add(t.getContent().substring(108));
                    } else
                    {
                        tweetsItem.add(t.getContent().substring(72));
                    }
                }
                else
                    tweetsItem.add(t.getContent().substring(36));
            }
            tweetsItem.add("posted by " + t.getEmiter() + " at " + t.getDate());
        }
    }
    
    @Override
    public void run() {
        //Check the SystemTray is supported
        DataHandler hData = new DataHandler(data);
        if(this.isSystraySupport()){
            constructMenu();
            new Thread(hData).start();
            while(true){
                feedTweets();
                try {
                    Thread.sleep(150000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            System.out.print("Systray not supported");
            System.exit(1);
        }
   }
    
}
