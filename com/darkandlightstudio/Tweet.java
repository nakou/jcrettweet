/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.darkandlightstudio;

/**
 *
 * @author Nakou | Twitter : Nakou - Mail : "nakou01[at]gmail.com"
 */
public class Tweet {
    private String emiter;
    private String content;
    private String date;
    
    public Tweet(String e, String c, String d){
        this.emiter = e;
        this.content = c;
        this.date = d;
    }
    
    @Override
    public String toString(){
        String s = null;
        s = "" + emiter + "" + content + "" + date;
        return s;
    }

    public String getEmiter() {
        return emiter;
    }

    public String getContent() {
        return content;
    }

    public String getDate() {
        return date;
    }
    
    
}
